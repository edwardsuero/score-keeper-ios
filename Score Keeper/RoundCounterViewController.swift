//
//  RoundCounterViewController.swift
//  Score Keeper
//
//  Created by edwardsuero on 12/1/15.
//  Copyright © 2015 Suero. All rights reserved.
//

import UIKit

class RoundCounterViewController: UIViewController {

    @IBOutlet var container: UIView!
    @IBOutlet var team1ScoreField: UITextField!
    @IBOutlet var team2ScoreField: UITextField!
    @IBOutlet var team2TotalScoreLabel: UILabel!
    @IBOutlet var team1TotalScoreLabel: UILabel!
    
    var tableViewController: UIViewController!
    var team1Array: NSMutableArray = []
    var team2Array: NSMutableArray = []
    var finalTeam1Array: NSArray?
    var finalTeam2Array: NSArray?
    var team1TotalScore: Int = 0
    var team2TotalScore: Int = 0
    
    override func viewDidLoad() {
       
        super.viewDidLoad()

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        tableViewController = storyboard.instantiateViewControllerWithIdentifier("roundCounterTableViewController")
        addChildViewController(tableViewController)
        tableViewController.didMoveToParentViewController(self)
        tableViewController.view.frame = self.container.bounds
        container.addSubview(tableViewController.view)
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Refresh, target: self, action: "newGamePressed")
        navigationItem.leftBarButtonItem?.title = ""
        
        navigationController?.navigationItem.leftBarButtonItem?.title = ""
        
        let defaults = NSUserDefaults.standardUserDefaults()
        let team1Object : AnyObject? = defaults.objectForKey(TEAM1_ROUND_SCORE_KEY)
        let team2Object : AnyObject? = defaults.objectForKey(TEAM2_ROUND_SCORE_KEY)
        if (team1Object != nil) {
            finalTeam1Array = team1Object! as? NSMutableArray
            for scoreItem in finalTeam1Array! {
                team1Array.addObject(scoreItem)
            }
        }
        if (team2Object != nil) {
            finalTeam2Array = team2Object! as? NSMutableArray
            for scoreItem in finalTeam2Array! {
                team2Array.addObject(scoreItem)
            }
        }

        updateScores()
        updateTableArrays()
        
        team1ScoreField.text = ""
        team2ScoreField.text = ""
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    override func viewDidLayoutSubviews() {
        updateTableArrays()
    }
    func newGamePressed() {
        clearScores()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        updateTableArrays()
        self.view.endEditing(true)
    }

    @IBAction func submit(sender: UIButton) {
        
        if(team1ScoreField.text == "") {
            team1Array.addObject(0)
        }
        else {
            team1Array.addObject(Int(team1ScoreField.text!)!)
        }
        
        if(team2ScoreField.text == "") {
            team2Array.addObject(0)
        }
        else {
            team2Array.addObject(Int(team2ScoreField.text!)!)
        }
        
        updateScores()
        updateTableArrays()
        
        team1ScoreField.text = ""
        team2ScoreField.text = ""
        
    }
    
    func updateTableArrays() {
        let controller = tableViewController as! RoundCounterTableViewController
        controller.updateTableArrays(team1Array, team2Array: team2Array)
    }
    
    func updateScores() {
        team1TotalScore = 0
        team2TotalScore = 0
        finalTeam1Array = team1Array
        finalTeam2Array = team2Array
        
        for roundScore in finalTeam1Array! {
            team1TotalScore += roundScore as! Int
        }
        for roundScore in finalTeam2Array! {
            team2TotalScore += roundScore as! Int
        }
        team1TotalScoreLabel.text = "Total: " + String(team1TotalScore)
        team2TotalScoreLabel.text = "Total: " + String(team2TotalScore)
        
    }
    
    func clearScores() {
        team1Array = []
        team2Array = []
        updateScores()
        updateTableArrays()
        team1ScoreField.text = ""
        team2ScoreField.text = ""
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(finalTeam1Array, forKey: TEAM1_ROUND_SCORE_KEY)
        defaults.setObject(finalTeam2Array, forKey: TEAM2_ROUND_SCORE_KEY)
        defaults.synchronize()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
