//
//  ScoreSheetCollectionViewController.swift
//  Score Keeper
//
//  Created by edwardsuero on 12/2/15.
//  Copyright © 2015 Suero. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

protocol ScoreSheetCollectionScrollDataDelegate {
    func setCollectionScrollData(scrollView: UIScrollView)
}

class ScoreSheetCollectionViewController: UICollectionViewController {
    
    var count = 0;
    var subViewFrame: CGRect?
    var delegate: ScoreSheetCollectionScrollDataDelegate?
    var tableData: NSArray?
    var populatedStore: NSMutableArray?
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
   }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        self.collectionView!.registerClass(ScoreSheetCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 150
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath)
    
        // Configure the cell
       // cell.cellLabel.text = "Test"
        
        let customCell = cell as! ScoreSheetCollectionViewCell
        
//        switch (color_selector++) {
//        case 0:
//            cell.backgroundColor = UIColor.blackColor()
//        case 1:
//            cell.backgroundColor = UIColor.blueColor()
//        case 2:
//            cell.backgroundColor = UIColor.greenColor()
//        case 3:
//            cell.backgroundColor = UIColor.redColor()
//        case 4:
//            cell.backgroundColor = UIColor.yellowColor()
//        case 5:
//            cell.backgroundColor = UIColor.orangeColor()
//        case 6:
//            cell.backgroundColor = UIColor.grayColor()
//        default:
//            break
//        }
//        if(color_selector > 5) {
//            color_selector = 0
//        }
        
        cell.layer.borderWidth = 0.5;
        cell.layer.borderColor = UIColor.grayColor().CGColor
        if(tableData != nil){
            if((indexPath.item < (tableData?.count)!)) {
                let string = (tableData![indexPath.item] as? String)!
                populatedStore?.insertObject(true, atIndex: indexPath.item)
                customCell.populate(string)
            }
        }
        return customCell
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
//        for subView in view.subviews {
//            if(subViewFrame != nil) {
//                if(CGRectEqualToRect(subView.frame,subViewFrame!)) {
//                    subView.removeFromSuperview()
//                    subViewFrame = nil
//                    return
//                }
//            }
//        }
//        
//        let cell = collectionView.cellForItemAtIndexPath(indexPath)
//        cell?.selected = true
//        cell?.backgroundColor = UIColor.grayColor()
//
//        let storyboard = UIStoryboard(name: "Main",bundle: nil)
//        let insertTextViewController = storyboard.instantiateViewControllerWithIdentifier("insertTextViewController")
//        
//
//        let sideLength = self.view.frame.size.width / 2
//        
//        let originX: CGFloat = self.view.frame.size.width/2 - sideLength/2
//        let originY: CGFloat
//        
//        if(cell!.frame.origin.y < self.view.frame.size.height/2) {
//            originY = self.view.frame.size.height*3/4 - sideLength/2
//        }
//        else {
//            originY = self.view.frame.size.height/4 - sideLength/2
//        }
//        
//        
//        subViewFrame = CGRect(x: originX, y: originY, width: sideLength, height: sideLength)
//        insertTextViewController.view.bounds = subViewFrame!
//        insertTextViewController.view.frame = subViewFrame!
//        insertTextViewController.view.layer.borderWidth = 1.0
//        insertTextViewController.view.layer.borderColor = UIColor.blackColor().CGColor
//        self.view.addSubview(insertTextViewController.view)
//        

//

        
        
        
    }
    
    override func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
//        let cell = collectionView.cellForItemAtIndexPath(indexPath)
        
//        cell?.selected = false
//        cell?.backgroundColor = UIColor.whiteColor()
        
    }
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
//        for subView in view.subviews {
//            if(subViewFrame != nil) {
//                if(CGRectEqualToRect(subView.frame,subViewFrame!)) {
//                    subView.removeFromSuperview()
//                    subViewFrame = nil
//                }
//            }
//        }
        delegate?.setCollectionScrollData(scrollView)
    }
    
    
    func setScoreData(data: NSMutableArray){
        for (var i=0; i < 150; i++) {
            populatedStore?.addObject(false)
        }
        tableData = data
    }

    
    
//    collectionView
    
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */

}
