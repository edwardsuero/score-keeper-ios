//
//  LoginScreenViewController.swift
//  Score Keeper
//
//  Created by edwardsuero on 12/5/15.
//  Copyright © 2015 Suero. All rights reserved.
//

import UIKit

class LoginScreenViewController: UIViewController, UIScrollViewDelegate, FBSDKLoginButtonDelegate {
    @IBOutlet var facebookButton: FBSDKLoginButton!
    
    @IBOutlet var mainView: UIView!
    @IBOutlet var scrollView: LoginScrollView!
    var originalCenterPoint: CGPoint?
    var keyboardHeight: CGFloat = 0

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        originalCenterPoint = view.center
        NSNotificationCenter.defaultCenter().addObserver(self, selector:Selector("keyboardShown:"), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:Selector("keyboardHidden:"), name: UIKeyboardWillHideNotification, object: nil)

        
        facebookButton.readPermissions = ["public_profile", "email", "user_friends"]
        facebookButton.layer.cornerRadius = 10
        facebookButton.delegate = self
        facebookButton.clipsToBounds = true
        
    

        // Do any additional setup after loading the view.
    }

    func loginButtonWillLogin(loginButton: FBSDKLoginButton!) -> Bool {
        return true
    }
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        
    }
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        if(!result.isCancelled && error == nil)
        {
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setBool(true, forKey: AUTHENTICATED_KEY)
            defaults.setObject(result.token.userID, forKey: USER_ID_KEY)
            defaults.synchronize()
            presentMainView()
        }
    }
    
    func presentMainView(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainTableViewNavigationController = storyboard.instantiateViewControllerWithIdentifier("mainTableViewNavigationController")
        self.presentViewController(mainTableViewNavigationController, animated: true, completion: nil)
    }
    
    @objc func keyboardShown(notification: NSNotification) {
        
        if(keyboardHeight == 0) {
            keyboardHeight = (notification.userInfo![UIKeyboardFrameEndUserInfoKey]?.CGRectValue.size.height)!
            scrollView.contentOffset.y = scrollView.contentOffset.y + keyboardHeight/3
        }
    }
    @objc func keyboardHidden(notification: NSNotification) {
        
        if(keyboardHeight != 0) {
            scrollView.contentOffset.y = scrollView.contentOffset.y - keyboardHeight/3
            keyboardHeight = 0
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
