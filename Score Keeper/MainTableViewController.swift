//
//  MainTableViewController.swift
//  Score Keeper
//
//  Created by edwardsuero on 12/1/15.
//  Copyright © 2015 Suero. All rights reserved.
//

import UIKit

class MainTableViewController: UITableViewController {

    var ROUND_SCORE_NAME = "Round Score"
    var SCORE_SHEET_NAME = "Leaderboard"
    var NUMBER_OF_SPORTS = 1
    var listOfSportsNames: NSMutableArray = []
    var listOfStatViewNames: NSMutableArray = []
    var facebookButton: FBSDKLoginButton?
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        initializeListOfSportsNames()
        initializeListOfStatViewNames()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 38.0/255.0, green: 61.0/255.0, blue: 62.0/255.0, alpha: 1.0) //x263D3E
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        let tableView = self.view as! UITableView
        tableView.backgroundColor = UIColor(red: 239/255.0, green: 245/255.0, blue: 245/255.0, alpha: 1.0) //xEFF5F5

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    @IBAction func logOutAction(sender: UIBarButtonItem) {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setBool(false, forKey: AUTHENTICATED_KEY)
        defaults.synchronize()
        FBSDKLoginManager().logOut()
        presentLoginView()
    }
    func presentLoginView(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginScreenViewController = storyboard.instantiateViewControllerWithIdentifier("loginScreenViewController")
        self.presentViewController(loginScreenViewController, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return NUMBER_OF_SPORTS
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("mainReuseIdentifier", forIndexPath: indexPath)
        
        let rowIndex = indexPath.row
        if(indexPath.section == 0){
            if(listOfSportsNames.count > rowIndex){
                cell.textLabel!.text = listOfSportsNames.objectAtIndex(rowIndex) as? String
            }
            else {
                cell.hidden = true
            }
        }
        else if(indexPath.section == 1) {
            if(listOfSportsNames.count > rowIndex){
                cell.textLabel!.text = listOfStatViewNames.objectAtIndex(rowIndex) as? String
            }
            else {
                cell.hidden = true
            }
        }

        return cell
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(section ==  0) {
            return "Sports/Games"
        }
        else if(section == 1){
            return "Statistics"
        }
        else {
            return ""
        }
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let height = CGFloat.init(integerLiteral: 28)
        return height
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let rowDeselectedIndex = indexPath.row

        if(indexPath.section == 0) {
            switch (rowDeselectedIndex) {
                case 0:
                    loadRoundScoreView()
                default:
                    break
            }
        } else if (indexPath.section == 1) {
            switch (rowDeselectedIndex) {
            case 0:
                loadScoreSheetView()
            default:
                break
            }
        }
    }
    
    
    /*
        Helper Functions
    */
    func initializeListOfSportsNames() {
        
        listOfSportsNames.addObject(ROUND_SCORE_NAME)
    }
    func initializeListOfStatViewNames() {
        
        listOfStatViewNames.addObject(SCORE_SHEET_NAME)
    }
    
    func loadRoundScoreView() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let roundScoreViewController = storyboard.instantiateViewControllerWithIdentifier("roundCounterViewController")
        navigationController?.pushViewController(roundScoreViewController, animated: true)
        roundScoreViewController.navigationController?.title = ROUND_SCORE_NAME
    }
    
    func loadScoreSheetView() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let scoreSheetViewController = storyboard.instantiateViewControllerWithIdentifier("scoreSheetViewController")
        navigationController?.pushViewController(scoreSheetViewController, animated: true)
        scoreSheetViewController.navigationController?.title = SCORE_SHEET_NAME
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
