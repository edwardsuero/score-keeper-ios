//
//  Score Keeper-Bridging-Header.h
//  Score Keeper
//
//  Created by edwardsuero on 12/5/15.
//  Copyright © 2015 Suero. All rights reserved.
//

#ifndef Score_Keeper_Bridging_Header_h
#define Score_Keeper_Bridging_Header_h

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#define AUTHENTICATED_KEY "authenticated_key"
#define USER_ID_KEY "user_id_key"
#define TEAM1_ROUND_SCORE_KEY "team1_round_score_key"
#define TEAM2_ROUND_SCORE_KEY "team2_round_score_key"

#endif /* Score_Keeper_Bridging_Header_h */
