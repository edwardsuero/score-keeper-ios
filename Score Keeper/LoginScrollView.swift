//
//  LoginScrollView.swift
//  Score Keeper
//
//  Created by edwardsuero on 12/5/15.
//  Copyright © 2015 Suero. All rights reserved.
//

import UIKit

class LoginScrollView: UIScrollView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.scrollEnabled = false
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.endEditing(true)
        resignFirstResponder()
    }

}
