//
//  VerticalTableViewController.swift
//  Score Keeper
//
//  Created by edwardsuero on 12/2/15.
//  Copyright © 2015 Suero. All rights reserved.
//

import UIKit

protocol ScoreSheetVerticalScrollDataDelegate {
    func setVerticalScrollData(scrollView: UIScrollView)
}

class VerticalTableViewController: UITableViewController {

    var delegate: ScoreSheetVerticalScrollDataDelegate?
    var tableData: NSArray?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 15
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("verticalReuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...
        
        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = UIColor.blackColor().CGColor
        if(indexPath.row < tableData?.count){
            cell.textLabel?.text = tableData![indexPath.row] as? String
            let boldFont = UIFont.boldSystemFontOfSize(UIFont.systemFontSize() - 3)
            cell.textLabel!.font = boldFont
        }
        return cell
    }
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y < -5) { //Limit bounce to 5 pixels
            scrollView.contentOffset = CGPointMake(0, -5);
        }

        delegate?.setVerticalScrollData(scrollView)
    }

    func setTeamNames(data: NSMutableArray) {
        tableData = data
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
