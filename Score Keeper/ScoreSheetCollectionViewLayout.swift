//
//  ScoreSheetCollectionViewLayout.swift
//  Score Keeper
//
//  Created by edwardsuero on 12/2/15.
//  Copyright © 2015 Suero. All rights reserved.
//

import UIKit

class ScoreSheetCollectionViewLayout: UICollectionViewFlowLayout {
    
    var currentBounds: CGRect = CGRect()
    var collectionViewFrame: CGRect = CGRect()
    var layoutInfo: NSDictionary = NSDictionary()
    var numColumns: Int = 10
    var numRows: Int = 15
    var cellHeight: Int
    var cellWidth: Int

    required init?(coder aDecoder: NSCoder) {
        
        let tempVerticalCell = UITableViewCell.init(style: UITableViewCellStyle.Default, reuseIdentifier: "verticalReuseIdentifier")
        let tempHorizontalCell = UITableViewCell.init(style: UITableViewCellStyle.Default, reuseIdentifier: "horizontalReuseIdentifier")
        cellHeight = Int(tempVerticalCell.frame.height)
        cellWidth = Int(tempHorizontalCell.frame.height)
        
        super.init(coder: aDecoder)
    }
    
    override func prepareLayout() {
        
        let cellLayoutInfo = NSMutableDictionary()
        let newLayoutInfo = NSMutableDictionary()
        
        collectionViewFrame = (self.collectionView?.frame)!
        let sectionCount = collectionView?.numberOfSections()
        var indexPath = NSIndexPath(forItem: 0, inSection:0)
        for(var section = 0; section < sectionCount; section++){
            let itemCount = collectionView?.numberOfItemsInSection(section)
            for(var item = 0; item < itemCount; item++){
                indexPath = NSIndexPath(forItem: item, inSection: section)
                let attributes = UICollectionViewLayoutAttributes(forCellWithIndexPath: indexPath)
                attributes.frame = calculateCellFrame(indexPath)
                cellLayoutInfo[indexPath] = attributes
            }
        }
        
        newLayoutInfo["cell"] = cellLayoutInfo
        
        self.layoutInfo = newLayoutInfo
    
    }
    override func collectionViewContentSize() -> CGSize {
        
        return CGSize(width: cellWidth*numColumns, height: cellHeight*numRows)
    }
    

    override func layoutAttributesForElementsInRect(rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        let allAttributes = NSMutableArray(capacity: self.layoutInfo.count)
        
        self.layoutInfo.enumerateKeysAndObjectsUsingBlock ({(elementIdentifier,elementsInfo, stop) -> Void in
            elementsInfo.enumerateKeysAndObjectsUsingBlock({(indexPath, attributes, innerStop)
            -> Void in
                if(CGRectIntersectsRect(rect, attributes.frame)) {
                    allAttributes.addObject(attributes)
                }
            })
            
        })
        
        
        return allAttributes as NSArray as? [UICollectionViewLayoutAttributes]
    }
    
    
    override func invalidationContextForBoundsChange(newBounds: CGRect) -> UICollectionViewLayoutInvalidationContext {
        let context = super.invalidationContextForBoundsChange(newBounds)
        
        return context
    }
    override func layoutAttributesForItemAtIndexPath(indexPath: NSIndexPath) -> UICollectionViewLayoutAttributes? {
        
        return self.layoutInfo["cell"]?[indexPath] as? UICollectionViewLayoutAttributes
    }
    
    override func shouldInvalidateLayoutForBoundsChange(newBounds: CGRect) -> Bool {
        
        invalidateLayoutWithContext(invalidationContextForBoundsChange(newBounds))
        return super.shouldInvalidateLayoutForBoundsChange(newBounds)
    }
    
    /*
        Methods for items being deleted.
    */
//    override func initialLayoutAttributesForAppearingItemAtIndexPath(itemIndexPath: NSIndexPath) -> UICollectionViewLayoutAttributes? {
//    
//        return self.layoutInfo["cell"]?[itemIndexPath] as? UICollectionViewLayoutAttributes
//    }
//    override func finalLayoutAttributesForDisappearingItemAtIndexPath(itemIndexPath: NSIndexPath) -> UICollectionViewLayoutAttributes? {
//        return self.layoutInfo["cell"]?[itemIndexPath] as? UICollectionViewLayoutAttributes
//    }
    
    /*
        Layout-related preperation method
//    */
//    override func prepareForCollectionViewUpdates(updateItems: [UICollectionViewUpdateItem]) {
//        
//        for updateItem in updateItems {
//            
//            switch updateItem.updateAction {
//                case .Insert:
//                    break
//                case .Delete:
//                    break
//                case .Reload:
//                    break
//                case .Move:
//                    break
//                case .None:
//                    break
//            }
//        }
//    }
    
    /*
        Animation; final Layout-related tasks
    */
//    override func finalizeCollectionViewUpdates() {
//        
//    }
    
    func calculateCellFrame(indexPath: NSIndexPath) -> CGRect {
        
        let row = indexPath.item / numColumns
        let column = indexPath.item % numColumns
        
        let originX = column == 0 ? 0 : (Int(column) * (cellWidth))
        let originY = row == 0 ? 0 : (Int(row) * (cellHeight))
        
        return CGRect(x: originX, y: originY, width: cellWidth, height: cellHeight)
    }
    
    /*
    Setter Methods
    */
    func setNumberOfRows(rows:Int) {
        if(numRows == rows) {
            return
        }
        numRows = rows
        self.invalidateLayout()
    }
    func setNumberOfColumns(columns:Int) {
        if(numColumns == columns){
            return
        }
        numColumns = columns
        self.invalidateLayout()
    }
}
