//
//  ScoreSheetCollectionViewCell.swift
//  Score Keeper
//
//  Created by edwardsuero on 12/3/15.
//  Copyright © 2015 Suero. All rights reserved.
//

import UIKit

class ScoreSheetCollectionViewCell: UICollectionViewCell {
    
    var textLabel: UITextField?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func populate(data: String) {
        let originX = bounds.origin.x + 5
        let originY = bounds.origin.y + 5
        let width = bounds.width - 10
        let height = bounds.height - 10
        let frame = CGRect(x: originX, y: originY, width: width, height: height)
        textLabel = UITextField(frame: frame)
        textLabel!.text = data
        textLabel!.font = UIFont.systemFontOfSize(UIFont.systemFontSize())
        for subView in self.subviews {
            if (CGRectEqualToRect(subView.frame,frame)) {
                subView.removeFromSuperview()
            }
        }
        addSubview(textLabel!)
    }
    
}
