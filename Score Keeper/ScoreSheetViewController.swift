//
//  ScoreSheetViewController.swift
//  Score Keeper
//
//  Created by edwardsuero on 12/2/15.
//  Copyright © 2015 Suero. All rights reserved.
//

import UIKit

class ScoreSheetViewController: UIViewController, ScoreSheetCollectionScrollDataDelegate, ScoreSheetHorizontalScrollDataDelegate, ScoreSheetVerticalScrollDataDelegate {
    
    @IBOutlet var horizontalContainer: UIView!
    @IBOutlet var verticalContainer: UIView!
    @IBOutlet var collectionContainer: UIView!
    
    var horizontalTableViewController: HorizontalTableViewController?
    var verticalTableViewController: VerticalTableViewController?
    var scoreSheetCollectionController: ScoreSheetCollectionViewController?
    
    var collectionScrollView, horizontalScrollView, verticalScrollView: UIScrollView?
    var scrollStateVar: scrollState?
    var SCORE_SHEET_NAME = "Leaderboard"
    
    enum scrollState {
        case Collection, Horizontal, Vertical
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        horizontalTableViewController = storyboard.instantiateViewControllerWithIdentifier("horizontalTableViewController") as? HorizontalTableViewController
        verticalTableViewController = storyboard.instantiateViewControllerWithIdentifier("verticalTableViewController") as? VerticalTableViewController
        scoreSheetCollectionController = storyboard.instantiateViewControllerWithIdentifier("scoreSheetCollectionViewController") as? ScoreSheetCollectionViewController
        self.navigationItem.title = SCORE_SHEET_NAME
        
        horizontalTableViewController!.view.frame = horizontalContainer.bounds
        verticalTableViewController!.view.frame = verticalContainer.bounds
        scoreSheetCollectionController!.view.frame = collectionContainer.bounds
        
        addChildViewController(horizontalTableViewController!)
        addChildViewController(verticalTableViewController!)
        addChildViewController(scoreSheetCollectionController!)
        
        horizontalScrollView = horizontalTableViewController?.view as? UIScrollView
        verticalScrollView = verticalTableViewController?.view as? UIScrollView
        collectionScrollView = scoreSheetCollectionController?.view as? UIScrollView
        
        horizontalTableViewController!.didMoveToParentViewController(self)
        verticalTableViewController!.didMoveToParentViewController(self)
        scoreSheetCollectionController!.didMoveToParentViewController(self)
        
        scoreSheetCollectionController!.delegate = self
        horizontalTableViewController!.delegate = self
        verticalTableViewController!.delegate = self
        
        collectionContainer.addSubview(scoreSheetCollectionController!.view)
        horizontalContainer.addSubview(horizontalTableViewController!.view)
        verticalContainer.addSubview(verticalTableViewController!.view)

        let defaults = NSUserDefaults.standardUserDefaults()
        if((defaults.objectForKey(USER_ID_KEY)) as! String == "1011890355500322") // my userID for demo
        {
            setSampleData()
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setCollectionScrollData(scrollView: UIScrollView) {
        //if(scrollStateVar == scrollState.Collection) {
        
            let offsetX = scrollView.contentOffset.x
            let offsetY = scrollView.contentOffset.y
            collectionScrollView = scrollView
            horizontalScrollView?.contentOffset = CGPoint(x: (horizontalScrollView?.contentOffset.x)!, y: offsetX)
            verticalScrollView?.contentOffset = CGPoint(x: (verticalScrollView?.contentOffset.x)!, y: offsetY)
        //}
        //scrollStateVar = scrollState.Collection
    }
    
    func setHorizontalScrollData(scrollView: UIScrollView) {
        //if(scrollStateVar == scrollState.Horizontal) {
            horizontalScrollView = scrollView
            let offsetX = scrollView.contentOffset.y
            let offsetY = collectionScrollView?.contentOffset.y
            collectionScrollView?.contentOffset = CGPoint(x: offsetX, y: offsetY!)
        //}
        //scrollStateVar = scrollState.Horizontal
    }
    
    func setVerticalScrollData(scrollView: UIScrollView) {
        //if(scrollStateVar == scrollState.Vertical) {
            verticalScrollView = scrollView
            let offsetX = collectionScrollView?.contentOffset.x
            let offsetY = scrollView.contentOffset.y
            collectionScrollView?.contentOffset = CGPoint(x: offsetX!, y: offsetY)
        //}
        //scrollStateVar = scrollState.Vertical
    }
    
    func setSampleData(){
        let teamData: NSMutableArray = []
        
        for (var i = 0; i<15; i++) {
            teamData.addObject("Team " + String(arc4random_uniform(150)))
        }
        
        let scoreCategories: NSMutableArray = []
        
        for (var i = 0; i<10; i++) {
            scoreCategories.addObject("PPG" + String(i))
        }
        
        let scoreData: NSMutableArray = []
        
        for (var i = 0; i<(15*10); i++) {
            scoreData.addObject(String(arc4random_uniform(800)))
        }
        
        horizontalTableViewController?.setScoreCategories(scoreCategories)
        verticalTableViewController?.setTeamNames(teamData)
        scoreSheetCollectionController?.setScoreData(scoreData)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
