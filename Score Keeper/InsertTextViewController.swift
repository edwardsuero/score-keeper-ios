//
//  InsertTextViewController.swift
//  Score Keeper
//
//  Created by edwardsuero on 12/5/15.
//  Copyright © 2015 Suero. All rights reserved.
//

import UIKit

class InsertTextViewController: UIViewController {

    var textField: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textField = UITextField(frame: view.frame)
        textField?.backgroundColor = UIColor.whiteColor()
        view.addSubview(textField!)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
